# overdrive-timer
A Bluetooth-based timer for Anki Overdrive supercar battery tests in demo mode

Make sure a car is switched off at least 30 seconds (configurable) before this script starts.
Timer starts when a car is switched on and ends 30 seconds (configurable) after it runs out of juice (or is switched off).
(Ctrl-C to quit).

Screenshot
----------

![Demo Screenshot](https://codeberg.org/harveyk/overdrive-timer/raw/branch/main/Screenshot_20220103_212119.png "Demo Screenshot")